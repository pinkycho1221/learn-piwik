# Piwik小範例

模擬商務網站埋入piwik記錄訂單的流程

### 啟動Http server

~~~bash
python -m SimpleHTTPServer 8000
~~~

### 連到網頁

打開瀏覽器，輸入[http://localhost:8000/www/index.html](http://localhost:8000/www/index.html)

